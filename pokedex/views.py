"""Pokedex view"""
from django.views.generic import ListView, DetailView
from pokedex.models import Pokemon
import random

class PokeAPIView(ListView):
    """Main view for PokeAPI"""

    template_name = 'pokeapi/index.html'
    model = Pokemon
    queryset = Pokemon.objects.filter(pokedex_number=random.randint(1,800)).first()
    context_object_name = 'pokemon'