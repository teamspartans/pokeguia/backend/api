"""Configure admin panel
"""
from django.contrib import admin
from .models import Pokemon, Type, Ability
from django.utils.html import mark_safe


class PokemonAdmin(admin.ModelAdmin):
    """Configuration for pokemon model

    Args:
        admin ([type]): [description]

    Returns:
        Generate pokemon model template
    """
    list_display = ('name', 'image_tag', 'description', 'type1', 'type2',
                    'strenght', 'legendary_percentage')
    date_hierarchy = 'created'
    list_filter = ["type1"]
    search_fields = ["name", "type1__name", "type2__name"]
    list_per_page = 10

    def has_add_permission(self, request):
        """Disable add permission"""
        return False

    def has_delete_permission(self, request, obj=None):
        """Disable delete permission"""
        return False


class TypeAdmin(admin.ModelAdmin):
    """Configuration for Type Admin

    Args:
        admin ([type]): [description]

    Returns:
        Generate pokemon type model template
    """
    list_display = ('name', 'color')
    date_hierarchy = 'created'
    search_fields = ["name"]
    readonly_fields = ["name"]

    def has_add_permission(self, request):
        """Disable add permission"""
        return False

    def has_delete_permission(self, request, obj=None):
        """Disable delete permission"""
        return False


class AbilityAdmin(admin.ModelAdmin):
    """Configuration for abilities admin

    Args:
        admin ([type]): [description]

    Returns:
        Generate pokemon abilities model template
    """
    list_display = ["name"]
    date_hierarchy = 'created'
    search_fields = ["name"]
    readonly_fields = ["name"]

    def has_add_permission(self, request):
        """Disable add permission"""
        return False

    def has_delete_permission(self, request, obj=None):
        """Disable delete permission"""
        return False

    def has_edit_permission(self, request, obj=None):
        """Disable edit permission"""
        return False


# Project configurations
admin.site.site_header = "PokeAdmin"
admin.site.site_title = 'PokeAdmin'
admin.site.index_title = 'Spartans'
admin.empty_value_display = '**Vacio**'

# Register models and configurations
admin.site.register(Pokemon, PokemonAdmin)
admin.site.register(Type, TypeAdmin)
admin.site.register(Ability, AbilityAdmin)
