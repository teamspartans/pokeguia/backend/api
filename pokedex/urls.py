# Django
from django.urls import path

# views
from pokedex import views

urlpatterns = [
    path(
        route='',
        view=views.PokeAPIView.as_view(),
        name='pokedex'
    ),
]
