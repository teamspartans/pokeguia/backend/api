"""Pokedex schema
    Query for the API
"""
import graphene
import django_filters
from graphene import Node
from graphene_django.types import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField
from .utils.utils import get_paginator
from pokedex.models import Pokemon, Type, Ability


class TypeType(DjangoObjectType):
    """Type for pokemon's types"""
    class Meta:
        """Properties for the type"""
        model = Type
        description = "Pokemon type catalog"


class AbilityType(DjangoObjectType):
    """Type for pokemon's Abilities"""
    class Meta:
        """Properties for the type"""
        model = Ability
        description = "Pokemon ability catalog"


class PokemonType(DjangoObjectType):
    """Type for pokemon's"""
    class Meta:
        """Properties for the type"""
        model = Pokemon
        description = "Pokemon attributes"


class PokemonPaginatedType(graphene.ObjectType):
    """Type for paginate pokemons"""
    page = graphene.Int()
    pages = graphene.Int()
    has_next = graphene.Boolean()
    has_prev = graphene.Boolean()
    data = graphene.List(PokemonType)

    class Meta:
        """Properties for the type"""
        model = Pokemon
        description = "Paginate pokemons"


class PokemonFilter(DjangoObjectType):
    """Type for filter pokemon's"""
    class Meta:
        """Properties for the type"""
        model = Pokemon
        interfaces = (graphene.relay.Node,)

        filter_fields = {
            "name": ["icontains"],
            "type1__name": ["exact"],
            "type2__name": ["exact"],
            'experience_growth': ['gt', 'lt'],
            'attack': ['gt', 'lt'],
            'sp_attack': ['gt', 'lt'],
            'pokedex_number': ['exact'],
            'generation': ['exact']
        }
        description = "Node with Pokemon Data"


class Query(object):
    """Querys for pokedex API"""

    pokemons = graphene.Field(PokemonPaginatedType,
                              page=graphene.Int(), limit=graphene.Int(), description="Get all paginated pokemons")

    pokemons_types_catalog = graphene.List(
        TypeType, description="Get all pokemon's types")
    pokemons_abilities_catalog = graphene.List(
        AbilityType, description="Get all pokemon's abilities")

    pokemons_search = DjangoFilterConnectionField(
        PokemonFilter, description="Search specific pokemon with optional params")
    pokemons_stronger = graphene.Field(PokemonPaginatedType,
                                       page=graphene.Int(), limit=graphene.Int(), description="Get stronger pokemon by IA model")
    pokemons_weaker = graphene.Field(PokemonPaginatedType,
                                     page=graphene.Int(), limit=graphene.Int(), description="Get weaker pokemon by IA model")
    pokemons_legendary = graphene.Field(PokemonPaginatedType,
                                        page=graphene.Int(), limit=graphene.Int(), description="Get legendary pokemon by IA model")

    def resolve_pokemons(self, info, page=1, limit=5, **kwargs):
        """Pokemons paginated"""
        if limit > 50:
            limit = 50
        qs = Pokemon.objects.all()
        return get_paginator(qs, limit, page, PokemonPaginatedType)

    def resolve_pokemons_stronger(self, info, page=1, limit=5, **kwargs):
        """Stronger pokemons"""
        if limit > 50:
            limit = 50
        qs = Pokemon.objects.all().order_by('-strenght')
        return get_paginator(qs, limit, page, PokemonPaginatedType)

    def resolve_pokemons_weaker(self, info, page=1, limit=5, **kwargs):
        """Weaker pokemons"""
        if limit > 50:
            limit = 50
        qs = Pokemon.objects.all().order_by('strenght')
        return get_paginator(qs, limit, page, PokemonPaginatedType)

    def resolve_pokemons_legendary(self, info, page=1, limit=5, **kwargs):
        """Legendary pokemons"""
        if limit > 50:
            limit = 50
        qs = Pokemon.objects.all().order_by('-legendary_percentage')
        return get_paginator(qs, limit, page, PokemonPaginatedType)

    def resolve_pokemons_types_catalog(self, info, **kwargs):
        """Catalog of types"""
        return Type.objects.all()

    def resolve_pokemons_abilities_catalog(self, info, **kwargs):
        """Catalog of abilities"""
        return Ability.objects.all()
