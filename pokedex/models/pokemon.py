"""Pokemon model
"""
from django.db import models
from .type import Type
from .ability import Ability
from django.utils.safestring import mark_safe


class Pokemon(models.Model):
    """Manage pokemons
    """

    name = models.CharField('name', max_length=140,
                            help_text='Name of the pokemon.')
    image = models.CharField('image', max_length=250,
                             help_text='Image of the pokemon.')

    description = models.TextField(
        'description', help_text='Short description of the pokemon.')

    hp = models.IntegerField('hp', help_text='Healthy Points.')
    attack = models.IntegerField('attack', help_text='Attack Points.')
    sp_attack = models.IntegerField(
        'sp_attack', help_text='The Base Attack of the Pokemon.')
    experience_growth = models.IntegerField(
        'experience_growth', help_text='The Experience Growth of the Pokemon.')
    defense = models.IntegerField('defense', help_text='Defense Points.')
    sp_defense = models.IntegerField(
        'sp_defense', help_text='The Base Special Defense of the Pokemon.')

    type1 = models.ForeignKey(
        Type, on_delete=models.CASCADE, related_name='pokemons_type1', help_text='The first type of the pokemon.')
    type2 = models.ForeignKey(
        Type, on_delete=models.CASCADE, related_name='pokemons_type2', help_text='The second type of the pokemon.')

    generation = models.IntegerField(
        'generation', help_text='The numbered generation which the Pokemon was first introduced.')
    pokedex_number = models.IntegerField(
        'pokedex_number', help_text='The entry number of the Pokemon in the National Pokedex.')
    abilities = models.CharField(
        'name', max_length=300, help_text='A stringified list of abilities that the Pokemon is capable of having.')

    ability1 = models.ForeignKey(
        Ability, on_delete=models.CASCADE, related_name='pokemons_ability1')
    ability2 = models.ForeignKey(
        Ability, on_delete=models.CASCADE, related_name='pokemons_ability2')
    ability3 = models.ForeignKey(
        Ability, on_delete=models.CASCADE, related_name='pokemons_ability3')
    ability4 = models.ForeignKey(
        Ability, on_delete=models.CASCADE, related_name='pokemons_ability4')
    ability5 = models.ForeignKey(
        Ability, on_delete=models.CASCADE, related_name='pokemons_ability5')
    ability6 = models.ForeignKey(
        Ability, on_delete=models.CASCADE, related_name='pokemons_ability6')

    strenght = models.FloatField(
        'strenght', help_text='Strenght of the pokemon.')

    legendary_percentage = models.FloatField(
        'legendary_percentage', help_text='Legendary percentage.'
    )

    created = models.DateTimeField(
        'created at',
        auto_now_add=True,
        help_text='Date time on which the object was created.'
    )
    modified = models.DateTimeField(
        'modified at',
        auto_now=True,
        help_text='Date time on which the object was last modified.'
    )

    def __str__(self):
        """Return a pokemon"""
        return self.name

    def image_tag(self):
        if self.image:
            return mark_safe(u'<img src="%s" style="width: 50px;" />' % self.image)
        else:
            return 'No Image Found'

    class Meta():
        """Meta class"""
        get_latest_by = 'created'
        ordering = ['-created', '-modified']
