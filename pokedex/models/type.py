from django.db import models
from colorfield.fields import ColorField


class Type(models.Model):
    """Types model
    Types for pokemons
    """

    name = models.CharField('name', max_length=140, help_text='Pokemon Type.')

    color = ColorField('color', default='#FF0000', help_text='Personalized Color Type.')

    created = models.DateTimeField(
        'created at',
        auto_now_add=True,
        help_text='Date time on which the object was created.'
    )
    modified = models.DateTimeField(
        'modified at',
        auto_now=True,
        help_text='Date time on which the object was last modified.'
    )

    def __str__(self):
        """Return a type name"""
        return self.name

    class Meta():
        """Meta class"""

        ordering = ['name', ]
