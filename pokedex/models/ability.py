from django.db import models


class Ability(models.Model):
    """Abilities model
    Abilities for pokemons
    """

    name = models.CharField('name', max_length=140, help_text='Pokemon abilities.')

    created = models.DateTimeField(
        'created at',
        auto_now_add=True,
        help_text='Date time on which the object was created.'
    )
    modified = models.DateTimeField(
        'modified at',
        auto_now=True,
        help_text='Date time on which the object was last modified.'
    )

    def __str__(self):
        """Return an ability name"""
        return self.name

    class Meta():
        """Meta class"""

        ordering = ['name', ]
