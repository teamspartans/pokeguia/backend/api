# API GraphQL <!-- omit in toc -->

An API to provide a pokemon data for the frontend application

<div align="center">
  <img src="core/img/pokespartans.png" height="30px">
  <b>Pokedex Project</b>
</div>

---

<div align="center">
	<img src="https://img.shields.io/badge/License-GPLv3-blue.svg"  alt="license badge">
    <img src="https://img.shields.io/pypi/pyversions/Django.svg?style=flat-square"  alt="python badge">
    <img src="https://img.shields.io/pypi/v/django.svg?label=django" alt="django badge">
    <img src="https://img.shields.io/pypi/v/graphene.svg?label=graphene" alt="django badge">
    <img src="https://img.shields.io/pypi/v/coverage.svg?label=coverage" alt="django badge">
</div>
<div align="center">
    <img alt="GitHub followers" src="https://img.shields.io/github/followers/eocode?label=eocode&style=social">
    <img alt="GitHub followers" src="https://img.shields.io/github/followers/KorKux1?label=KorKux1&style=social">
</div>
<div align="center">
    <img alt="Twitter URL" src="https://img.shields.io/twitter/url?label=eocode&style=social&url=https%3A%2F%2Ftwitter.com%2Feocode">
</div>

## Demo

If you want to see the demo of this project deployed, you can visit it here

https://blissful-mile-280405.ue.r.appspot.com

Up until June 2020

## How to clone
You can clone the repository

    $ git clone https://gitlab.com/pokespartans/backend/api.git

## Installation
To install this project just type

Create virtual enviroment:

    $ python -m venv env

Active your enviroment

Install dependencies
    
    $ pip install -r requirements.txt
    $ pip install -r core/requirements/development.txt

Compile statics

    $ python manage.py collectstatic

Run the project

    $ python manage.py runserver

## File structure

* **core** (Main module and configuration for project)
  * **enviroments** (Files to build ci/cd)
  * **img** (Readme images for project)
  * **requirements** (python dependencies for development and testing)
  * **static** (Static files for Django app)
  * **templates** (Override original template files)
* **pokedex** (Pokemon module of API)
* **transfer** (Sync module into DW to APP)

## Development configuration

On root folder create a env.yaml file

```yaml
env_variables:
  host: yourhost
  user: yourusername
  password: yourpassword
  database: yourdatabasename
```

```shell
python manage.py migrate
```

## Production enviroment

* Merge or send a pull request on master for deploy

## Preview

### Pokeadmin

https://blissful-mile-280405.ue.r.appspot.com/admin/

<div align="center">
  <img src="core/img/admin.png">
</div>

### Main Page

https://blissful-mile-280405.ue.r.appspot.com

<div align="center">
  <img src="core/img/api.png">
</div>

### API with docs

https://blissful-mile-280405.ue.r.appspot.com/graphql/

<div align="center">
  <img src="core/img/graphql.png">
</div>

### Transfer / Pokemon Sync

#### Catalogs

https://blissful-mile-280405.ue.r.appspot.com/sincronize/catalogs

#### Pokemons

https://blissful-mile-280405.ue.r.appspot.com/sincronize/pokemons

## Testing

See coverage testing run:

```shell
coverage run manage.py test core
coverage html
```

## How to contribute

* Review the [code of conduct](https://gitlab.com/pokespartans/code-of-conduct) to contribute to the project
* You can create a Pull request to the project

## License

GNU GENERAL PUBLIC LICENSE
Version 3