import yaml
import os

def get_dev_connection():
    with open('env.yaml') as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
        
    return {
        'default': {
            'ENGINE': 'django.db.backends.postgresql',
            'NAME': data['env_variables']['database'],
            'USER': data['env_variables']['user'],
            'PASSWORD': data['env_variables']['password'],
            'HOST': data['env_variables']['host'],
            'PORT': '5432',
        }
    }
    
def get_production_connection():
    return {
        'default': {
            'ENGINE': 'django.db.backends.postgresql',
            'NAME': os.environ.get("database"),
            'USER': os.environ.get("user"),
            'PASSWORD': os.environ.get("password"),
            'HOST': os.environ.get("host"),
            'PORT': '5432',
        }
    }