"""Main Schema
"""
import graphene
from graphene_django.debug import DjangoDebug
import pokedex.schema


class Query(pokedex.schema.Query, graphene.ObjectType):
    """The Query root of PokeAPI.
    Spartans Team
    """
    pass


schema = graphene.Schema(query=Query)
