from core.enviroments.connect import get_production_connection
from .base import *

DEBUG = False

ALLOWED_HOSTS = [
    'blissful-mile-280405.ue.r.appspot.com',
]

DATABASES = get_production_connection()

INSTALLED_APPS += [
    'corsheaders',
]

MIDDLEWARE += [
    'corsheaders.middleware.CorsMiddleware',
]

CORS_ORIGIN_ALLOW_ALL = True

CORS_ALLOW_CREDENTIALS = True
