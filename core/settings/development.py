from core.enviroments.connect import get_dev_connection
from .base import *

DEBUG = True

DATABASES = get_dev_connection()

ALLOWED_HOSTS = []

INSTALLED_APPS += [
    'corsheaders',
    'debug_toolbar',
    'graphiql_debug_toolbar',
]

MIDDLEWARE += [
    'corsheaders.middleware.CorsMiddleware',
    # 'debug_toolbar.middleware.DebugToolbarMiddleware',
    'graphiql_debug_toolbar.middleware.DebugToolbarMiddleware',
]

CORS_ORIGIN_ALLOW_ALL = True

CORS_ALLOW_CREDENTIALS = True

INTERNAL_IPS = [
    '127.0.0.1',
]

import socket

hostname, _, ips = socket.gethostbyname_ex(socket.gethostname())
INTERNAL_IPS += [ip[:-1] + '1' for ip in ips]