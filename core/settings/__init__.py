import sys

TESTING = len(sys.argv) > 1 and sys.argv[1] == 'test'
COMMAND = len(sys.argv) > 1 and (sys.argv[1] == 'makemigrations' or sys.argv[1] == 'migrate' or sys.argv[1] == 'createsuperuser' or sys.argv[1] == 'shell')
DEVELOPMENT = len(sys.argv) > 1 and sys.argv[1] == 'runserver'

if DEVELOPMENT:
    print('Run as Development')
    from .development import *
else:
    if TESTING:
        print('Run as Testing')
        from .development import *
    else:
        if COMMAND:
            print('Run as Command')
            from .development import *
        else:
            print('Run as Production')
            from .production import *
