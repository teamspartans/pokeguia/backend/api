"""Sincronize data to DW
"""

import psycopg2
from django.http import HttpResponse
from django.shortcuts import render
from django.conf import settings
from pokedex.models import Pokemon, Type, Ability


def sincronize_catalogs(request):
    """Get types and habilities catalogs from DW and insert into app

    Args:
        request ([type]): [description] 

    Returns:
        HTTP Status
    """
    try:
        connection = psycopg2.connect(user=settings.DATABASES['default']['USER'],
                                      password=settings.DATABASES['default']['PASSWORD'],
                                      host=settings.DATABASES['default']['HOST'],
                                      port="5432",
                                      database='DW')
        cursor = connection.cursor()
        select = "SELECT * FROM public.\"POKEMON_TYPES\";"
        cursor.execute(select)
        records = cursor.fetchall()

        for record in records:
            if Type.objects.filter(name=record[1]).count() == 0:
                Type.objects.create(
                    id=record[0], name=record[1], color=record[2])

        select = "SELECT * FROM public.\"POKEMON_ABILITIES\";"
        cursor.execute(select)
        records = cursor.fetchall()

        for record in records:
            if Ability.objects.filter(name=record[1]).count() == 0:
                Ability.objects.create(id=record[0], name=record[1])
        return HttpResponse('Sincronización correcta')
    except (Exception, psycopg2.Error) as error:
        return HttpResponse("Error de conección: "+select+' '+str(error))
    finally:
        # closing database connection.
        if(connection):
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")


def sincronize_pokemons(request):
    """Get Pokemons from DW and insert into app

    Args:
        request ([type]): [description] 

    Returns:
        HTTP Status
    """
    try:
        connection = psycopg2.connect(user=settings.DATABASES['default']['USER'],
                                      password=settings.DATABASES['default']['PASSWORD'],
                                      host=settings.DATABASES['default']['HOST'],
                                      port="5432",
                                      database='DW')
        cursor = connection.cursor()
        select = "SELECT * FROM public.\"POKEMONS\";"
        cursor.execute(select)
        records = cursor.fetchall()

        for record in records:
            print(record)
            if Pokemon.objects.filter(name=record[1]).count() == 0:
                t1 = Type.objects.get(id=record[6])
                t2 = Type.objects.get(id=record[7])
                a1 = Ability.objects.get(id=record[14])
                a2 = Ability.objects.get(id=record[15])
                a3 = Ability.objects.get(id=record[16])
                a4 = Ability.objects.get(id=record[17])
                a5 = Ability.objects.get(id=record[18])
                a6 = Ability.objects.get(id=record[19])
                Pokemon.objects.create(id=record[0],
                                       name=record[1],
                                       image=record[11],
                                       description=record[10],
                                       hp=record[2],
                                       attack=record[3],
                                       sp_attack=record[4],
                                       experience_growth=record[5],
                                       defense=record[8],
                                       sp_defense=record[9],
                                       type1=t1,
                                       type2=t2,
                                       generation=record[12],
                                       pokedex_number=record[13],
                                       abilities=record[20],
                                       ability1=a1,
                                       ability2=a2,
                                       ability3=a3,
                                       ability4=a3,
                                       ability5=a4,
                                       ability6=a5,
                                       strenght=record[22],
                                       legendary_percentage=record[21]
                                       )
        return HttpResponse('Sincronización correcta')
    except (Exception, psycopg2.Error) as error:
        return HttpResponse("Error de conección: "+select+' '+str(error))
    finally:
        # closing database connection.
        if(connection):
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")
