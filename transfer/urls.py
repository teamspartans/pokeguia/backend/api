# Django
from django.urls import path

# views
from transfer.views import sincronize_pokemons, sincronize_catalogs

urlpatterns = [
    path("sincronize/catalogs", sincronize_catalogs, name="catalogs"),
    path("sincronize/pokemons", sincronize_pokemons, name="pokemons"),
]
